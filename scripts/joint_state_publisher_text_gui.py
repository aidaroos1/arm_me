#!/usr/bin/env python3

import signal
import sys
import threading

import rospy
from sensor_msgs.msg import JointState
from python_qt_binding.QtWidgets import QApplication, QMainWindow, QVBoxLayout, QWidget, QLineEdit, QPushButton, QLabel
from python_qt_binding.QtCore import pyqtSlot

import joint_state_publisher
import time

class JointStatePublisherTextGui(QMainWindow):
    def __init__(self, title, jsp, num_rows):
        super(JointStatePublisherTextGui, self).__init__()
        self.setWindowTitle(title)
        self.jsp = jsp

        self.joint_state_pub = rospy.Publisher('/joint_states', JointState, queue_size=10)
        self.joint_positions = {}
        self.lock = threading.Lock()

        central_widget = QWidget()
        self.setCentralWidget(central_widget)
        layout = QVBoxLayout()
        central_widget.setLayout(layout)

        self.joint_texts = []

        for joint_name in self.jsp.free_joints:
            label = QLabel(joint_name)
            text_edit = QLineEdit()
            text_edit.setPlaceholderText(f"Enter value for {joint_name}")
            layout.addWidget(label)
            layout.addWidget(text_edit)
            self.joint_texts.append((joint_name, text_edit))

        self.update_button = QPushButton("Update Joint States")
        self.update_button.clicked.connect(self.update_joint_states)
        layout.addWidget(self.update_button)

        self.running = True
        self.publish_thread = threading.Thread(target=self.publish_joint_states)
        self.publish_thread.start()

    @pyqtSlot()
    def update_joint_states(self):
        with self.lock:
            for joint_name, text_edit in self.joint_texts:
                try:
                    value = float(text_edit.text())
                    self.joint_positions[joint_name] = value
                except ValueError:
                    rospy.logwarn(f"Invalid value for joint '{joint_name}': {text_edit.text()}")

            rospy.loginfo(f"Updated joint positions: {self.joint_positions}")

    def publish_joint_states(self):
        rate = rospy.Rate(40)  # 10 Hz
        while self.running:
            joint_state_msg = JointState()
            joint_state_msg.header.stamp = rospy.Time.now()
            with self.lock:
                joint_state_msg.name = list(self.joint_positions.keys())
                joint_state_msg.position = list(self.joint_positions.values())
            self.joint_state_pub.publish(joint_state_msg)
            rate.sleep()

    def closeEvent(self, event):
        self.running = False
        self.publish_thread.join()
        event.accept()

if __name__ == '__main__':
    try:
        rospy.init_node('joint_state_publisher_text_gui')
        app = QApplication(sys.argv)
        app.setApplicationDisplayName("Joint State Publisher")

        jsp = joint_state_publisher.JointStatePublisher()
        jsp_gui = JointStatePublisherTextGui("Node: " + rospy.get_name(), jsp, 0)
        jsp_gui.show()

        threading.Thread(target=jsp.loop).start()
        signal.signal(signal.SIGINT, signal.SIG_DFL)
        sys.exit(app.exec_())

    except rospy.ROSInterruptException:
        pass

