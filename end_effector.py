#!/usr/bin/env python3
import rospy
import numpy as np
import math
from sensor_msgs.msg import JointState
from geometry_msgs.msg import PointStamped
from urdf_parser_py.urdf import URDF

class EndEffectorPositionCalculator:
    def __init__(self):
        rospy.init_node('end_effector')
        self.robot = URDF.from_parameter_server()
        self.joint_subscriber = rospy.Subscriber('/joint_states', JointState, self.joint_callback)
        self.end_effector_publisher = rospy.Publisher('/end_effector_position', PointStamped, queue_size=10)

    def joint_callback(self, joint_state):
       # Convert radians to degrees for each joint angle if necessary for display or logging
        joint_angles_degrees = {name: math.degrees(angle) for name, angle in zip(joint_state.name, joint_state.position)}
        joint_angles_radians = {name: angle for name, angle in zip(joint_state.name, joint_state.position)}  # keep radian for calculations
        end_effector_position = self.compute_end_effector_position(joint_angles_radians)
        self.publish_end_effector_position(end_effector_position)

    def dh_transform(self, theta, d, a, alpha):
        """Create the Denavit-Hartenberg transformation matrix"""
        theta = math.radians(theta) 
        ct = np.cos(theta)
        st = np.sin(theta)
        ca = np.cos(alpha)
        sa = np.sin(alpha)
        return np.array([
            [ct, -st*ca, st*sa, a*ct],
            [st, ct*ca, -ct*sa, a*st],
            [0, sa, ca, d],
            [0, 0, 0, 1]
        ])

    def compute_end_effector_position(self, joint_angles):
        # Retrieve joint angles directly 
        theta1 = joint_angles.get('joint_1', 0)
        theta2 = joint_angles.get('joint_2', 0)
        theta3 = joint_angles.get('joint_3', 0)
        theta4 = joint_angles.get('joint_4', 0)
        theta5 = joint_angles.get('joint_5', 0)
        theta6 = joint_angles.get('joint_6', 0)

        # Define DH parameters for each joint (theta, d, a, alpha)
        T0 = self.dh_transform(0, 0.225, 0, 0)
        T1 = self.dh_transform(theta1, 0.1745, 0, -np.pi/2)
        T2 = self.dh_transform(theta2, 0, 0.34, 0)
        T3 = self.dh_transform(theta3, 0, 0, -np.pi/2)
        T4 = self.dh_transform(theta4, 0.36925, 0, np.pi/2)
        T5 = self.dh_transform(theta5, 0, 0, -np.pi/2)
        T6 = self.dh_transform(theta6, 0.089, 0, 0)

        # Composite transformation matrix
        T01 = T0 @ T1
        T02 = T01 @ T2
        T03 = T02 @ T3
        T04 = T03 @ T4
        T05 = T04 @ T5
        T06 = T05 @ T6
        print(T06)

        # Extract the position from the last column of the transformation matrix
        position = T06[:3, 3]  # Get the x, y, z positions
        return position

    def publish_end_effector_position(self, position):
        msg = PointStamped()
        msg.header.stamp = rospy.Time.now()
        msg.header.frame_id = "base_link"
        decimal_places = 3
        msg.point.x = round(position[0], decimal_places)
        msg.point.y = round(position[1], decimal_places)
        msg.point.z = round(position[2], decimal_places)
        self.end_effector_publisher.publish(msg)

if __name__ == '__main__':
    calculator = EndEffectorPositionCalculator()
    rospy.spin()

