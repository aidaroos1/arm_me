#!/usr/bin/env python3

import rospy
import tf

def tf_listener():
    rospy.init_node('tf_listener')
    listener = tf.TransformListener()
    rate = rospy.Rate(10.0)

    while not rospy.is_shutdown():
        try:
            (trans, rot) = listener.lookupTransform('base_link', 'link_effector', rospy.Time(0))
            rospy.loginfo("Translation: %s", str(trans))
            rospy.loginfo("Rotation: %s", str(rot))
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            continue

        rate.sleep()

if __name__ == '__main__':
    try:
        tf_listener()
    except rospy.ROSInterruptException:
        pass

