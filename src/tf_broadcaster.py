#!/usr/bin/env python3

import rospy
import tf
from sensor_msgs.msg import JointState
from geometry_msgs.msg import TransformStamped

class TFJointStateBroadcaster:
    def __init__(self):
        rospy.init_node('tf_broadcaster')
        self.br = tf.TransformBroadcaster()
        self.joint_sub = rospy.Subscriber('/joint_states', JointState, self.joint_state_callback)
        self.joint_state = None
        self.rate = rospy.Rate(20)  # 10 Hz

    def joint_state_callback(self, msg):
        self.joint_state = msg

    def dh_transform(self, theta, d, a, alpha):
        """Create the Denavit-Hartenberg transformation matrix"""
        theta = math.radians(theta)
        ct = np.cos(theta)
        st = np.sin(theta)
        ca = np.cos(alpha)
        sa = np.sin(alpha)
        return np.array([
            [ct, -st*ca, st*sa, a*ct],
            [st, ct*ca, -ct*sa, a*st],
            [0, sa, ca, d],
            [0, 0, 0, 1]
        ])

    def compute_transform(self, joint_angles):
        # Retrieve joint angles directly
        theta1 = joint_angles.get('joint_1', 0)
        theta2 = joint_angles.get('joint_2', 0)
        theta3 = joint_angles.get('joint_3', 0)
        theta4 = joint_angles.get('joint_4', 0)
        theta5 = joint_angles.get('joint_5', 0)
        theta6 = joint_angles.get('joint_6', 0)

        # Define DH parameters for each joint (theta, d, a, alpha)
        T0 = self.dh_transform(0, 0.225, 0, 0)
        T1 = self.dh_transform(theta1, 0.1745, 0, -np.pi/2)
        T2 = self.dh_transform(theta2, 0, 0.34, 0)
        T3 = self.dh_transform(theta3, 0, 0, -np.pi/2)
        T4 = self.dh_transform(theta4, 0.36925, 0, np.pi/2)
        T5 = self.dh_transform(theta5, 0, 0, -np.pi/2)
        T6 = self.dh_transform(theta6, 0.089, 0, 0)

        # Composite transformation matrix
        T01 = T0 @ T1
        T02 = T01 @ T2
        T03 = T02 @ T3
        T04 = T03 @ T4
        T05 = T04 @ T5
        T06 = T05 @ T6

        return T06

    def broadcast_transforms(self):
        while not rospy.is_shutdown():
            if self.joint_state:
                joint_angles = {name: angle for name, angle in zip(self.joint_state.name, self.joint_state.position)}
                T06 = self.compute_transform(joint_angles)

                t = TransformStamped()
                t.header.stamp = rospy.Time.now()
                t.header.frame_id = "base_link"
                t.child_frame_id = "link_effector"
                t.transform.translation.x = T06[0, 3]
                t.transform.translation.y = T06[1, 3]
                t.transform.translation.z = T06[2, 3]

                # Convert the rotation matrix to quaternion
                rot = tf.transformations.quaternion_from_matrix(T06)
                t.transform.rotation.x = rot[0]
                t.transform.rotation.y = rot[1]
                t.transform.rotation.z = rot[2]
                t.transform.rotation.w = rot[3]

                self.br.sendTransform(
                    (t.transform.translation.x, t.transform.translation.y, t.transform.translation.z),
                    (t.transform.rotation.x, t.transform.rotation.y, t.transform.rotation.z, t.transform.rotation.w),
                    t.header.stamp,
                    t.child_frame_id,
                    t.header.frame_id
                )

            self.rate.sleep()

if __name__ == '__main__':
    try:
        broadcaster = TFJointStateBroadcaster()
        broadcaster.broadcast_transforms()
    except rospy.ROSInterruptException:
        pass

